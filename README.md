# Wordwar 2.0

## The purpose of this simple game is to win!
First, after starting the game by ./wordwar _yourword_ where _yourword_ is minimal 1 and maximum 5 chars long. 
After pressing ENTER the computer will generate a random counter-word _computerword_ with a maximum five characters
Characters that coincide in both words, will be eliminated from the computers' input and from your input

If characters are all eliminated of _yourword_, the opponent will win
If characters are all eliminated from _computerword_ and _yourword_ in the same turn, the game will result in a draw
You win if all characters are eliminated from _computerword_

# Have fun!

